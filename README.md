#Nuget Package Config Manager  
  
This enables you to manage dependencies of multiple projects in a single easy to use interface.  
   
## How to  
Click the Select folder button and browse to the folder containg projects/solutions with nuget references (the search is recursive).  
Make changes in the grid (use the right click for some cool features).  
Click the Write button.  
Now if you have enable package restore on the projects they will sync to their new packages.config file on next build.  



