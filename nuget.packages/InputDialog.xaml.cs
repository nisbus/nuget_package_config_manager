﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace nuget.packages
{
    /// <summary>
    /// Interaction logic for InputDialog.xaml
    /// </summary>
    public partial class InputDialog : Elysium.Theme.Controls.Window, INotifyPropertyChanged
    {
        private string header;
        public string Header 
        {
            get { return header; }
            set 
            { 
                header = value; 
                if(PropertyChanged != null) 
                    PropertyChanged(this, new PropertyChangedEventArgs("Header")); 
            }
        }
        private string message;
        public string Message
        {
            get { return message; }
            set
            {
                message = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Message"));
            }
        }

        private string result;
        public string Result
        {
            get { return result; }
            set
            {
                result = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Result"));
            }
        }

        public InputDialog(string header, string message)
        {
            InitializeComponent();
            Header = header;
            Message = message;
            this.DataContext = this;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
