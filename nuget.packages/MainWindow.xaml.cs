﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Linq;
using ReactiveUI;
using Elysium.Theme;
using MahApps.Metro.Controls;

namespace nuget.packages
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow, INotifyPropertyChanged
    {
        #region Fields
        
        Subject<NugetPackage> packageSubscription = new Subject<NugetPackage>();

        #endregion

        #region Properties

        string _Path;
        public string Path
        {
            get { return _Path; }
            set
            {
                _Path = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Path"));
                if (!string.IsNullOrEmpty(value))
                    ScanFolder();
            }
        }

        public ReactiveCollection<NugetPackage> Packages { get; set; }
        bool loading;
        public bool Loading 
        {
            get { return loading; }
            set 
            {

                loading = value;

                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Loading"));
                    PropertyChanged(this, new PropertyChangedEventArgs("NotLoading"));
                }
            }
        }
        public bool NotLoading { get { return !Loading; } }

        #endregion

        #region CTOR

        public MainWindow()
        {
            InitializeComponent();            
            Packages = new ReactiveCollection<NugetPackage>();

            packageSubscription.ObserveOnDispatcher().Subscribe(x => Packages.Add(x));

            this.DataContext = this;
        }

        #endregion

        private void ScanFolder()
        {

            Loading = true;
            Packages.Clear();
            Task.Factory.StartNew<IEnumerable<NugetPackage>>(() =>
            {
                IEnumerable<NugetPackage> packUnion = new List<NugetPackage>();
                var packagesFiles = Directory.GetFiles(Path, "packages.config", SearchOption.AllDirectories).ToList();
                foreach (var file in packagesFiles)
                {
                    XDocument doc = XDocument.Load(file);
                    List<NugetPackage> packages = new List<NugetPackage>();
                    foreach (XElement p in doc.Root.DescendantNodes())
                    {
                        var id = p.Attribute("id") != null ? p.Attribute("id").Value : string.Empty;
                        var version = p.Attribute("version") != null ? p.Attribute("version").Value : string.Empty;
                        var allowed = p.Attribute("allowedVersions") != null ? p.Attribute("allowedVersions").Value : string.Empty;
                        var framework = p.Attribute("targetFramework") != null ? p.Attribute("targetFramework").Value : string.Empty;
                        packages.Add(new NugetPackage(id, version, allowed, framework, file));
                    }
                    packUnion = packages.Union(packUnion).OrderByDescending(x => x.Id);
                }
                return packUnion;
            }).ContinueWith((packs) =>
            {
                foreach (var p in packs.Result)
                    packageSubscription.OnNext(p);
                Loading = false;
            });
        }

        #region Button and menu clicks

        private void btnFolderSelectClick(object sender, RoutedEventArgs e)
        {
            var dlg = new FolderBrowserDialog();
            var result = dlg.ShowDialog(this.GetIWin32Window());
            if (result == System.Windows.Forms.DialogResult.OK)
                Path = dlg.SelectedPath;
        }

        private void SmartMergeClick(object sender, RoutedEventArgs e)
        {
            var selected = packageGrid.SelectedItems.OfType<NugetPackage>();
            if (selected.Count() > 0 && selected.All(x => x.Id == selected.First().Id))
            {
                var highest = selected.Max(x => x.Version);
                var allowed = selected.Where(x => !string.IsNullOrEmpty(x.AllowedVersions)).Min(x => x.AllowedVersions);
                var target = selected.Max(x => x.TargetFramework);
                foreach (var package in selected)
                {
                    package.Version = highest;
                    package.TargetFramework = target;
                    package.AllowedVersions = allowed;
                }
            }
        }

        private void SmartMergeAllClick(object sender, RoutedEventArgs e)
        {
            var groups = Packages.GroupBy(x => x.Id);
            foreach (var g in groups)
            {
                var selected = Packages.Where(x => x.Id == g.Key);
                var highest = selected.Max(x => x.Version);
                var allowed = selected.Where(x => !string.IsNullOrEmpty(x.AllowedVersions)).Min(x => x.AllowedVersions);
                var target = selected.Max(x => x.TargetFramework);
                foreach (var package in selected)
                {
                    package.Version = highest;
                    package.TargetFramework = target;
                    package.AllowedVersions = allowed;
                }
            }
        }

        private void MergeVersionsClick(object sender, RoutedEventArgs e)
        {
            var selected = packageGrid.SelectedItems.OfType<NugetPackage>();            
            if (selected.Count() > 0 && selected.All(x => x.Id == selected.First().Id))
            {
                var highest = selected.Max(x => x.Version);
                foreach (var package in selected)
                    package.Version = highest;
            }
        }

        private void SpecificVersionClick(object sender, RoutedEventArgs e)
        {
            var selected = packageGrid.SelectedItems.OfType<NugetPackage>();
            if (selected.Count() > 0 && selected.All(x => x.Id == selected.First().Id))
            {
                InputDialog dlg = new InputDialog("Confirm", "Type the version");
                if ((bool)dlg.ShowDialog())
                {
                    foreach (var p in selected)
                        p.Version = dlg.Result;
                }
            }
        }

        private void TargetFrameworkClick(object sender, RoutedEventArgs e)
        {
            var selected = packageGrid.SelectedItems.OfType<NugetPackage>();
            if (selected.Count() > 0 && selected.All(x => x.Id == selected.First().Id))
            {
                InputDialog dlg = new InputDialog("Confirm", "Type the target framework");
                if ((bool)dlg.ShowDialog())
                {
                    foreach (var p in selected)
                        p.TargetFramework = dlg.Result;
                }
            }
        }

        private void AllowedVersionsClick(object sender, RoutedEventArgs e)
        {
            var selected = packageGrid.SelectedItems.OfType<NugetPackage>();
            if (selected.Count() > 0 && selected.All(x => x.Id == selected.First().Id))
            {
                InputDialog dlg = new InputDialog("Confirm", "Type the allowed versions");
                if ((bool)dlg.ShowDialog())
                {
                    foreach (var p in selected)
                        p.AllowedVersions = dlg.Result;
                }
            }
        }

        private void RestrictToCurrentClick(object sender, RoutedEventArgs e)
        {
            var selected = packageGrid.SelectedItems.OfType<NugetPackage>();
            if (selected.Count() > 0 && selected.All(x => x.Id == selected.First().Id))
            {
                var highest = selected.Max(x => x.Version);
                foreach (var package in selected)
                {
                    package.AllowedVersions = "["+highest+"]";
                }
            }
        }

        private void RestrictAllToCurrentClick(object sender, RoutedEventArgs e)
        {
            var selected = packageGrid.SelectedItems.OfType<NugetPackage>();
            if (selected.Count() > 0 && selected.All(x => x.Id == selected.First().Id))
            {
                var highest = selected.Max(x => x.Version);
                foreach (var package in selected)
                {
                    package.AllowedVersions = "[" + highest + "]";
                }
            }
        }

        private void btnWriteClick(object sender, RoutedEventArgs e)
        {
            Loading = true;
            var selected = packageGrid.SelectedItems.OfType<NugetPackage>();
            foreach (var s in selected.GroupBy(x => x.Project))
            {
                var file = s.Key;
                var projectPackages = Packages.Where(x => x.Project == file);
                if (projectPackages.Count() > 0)
                {                    
                    var newFile = projectPackages.GetPackageString();
                    using (var f = File.Create(file))
                    {
                        var fileAsBytes = Encoding.UTF8.GetBytes(newFile);
                        f.Write(fileAsBytes, 0, fileAsBytes.Length);
                    }                    
                }
            }
            Loading = false;
        }

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
    }

    public static class EnumerableExtensions
    {
        public static string GetPackageString(this IEnumerable<NugetPackage> source)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(@"<?xml version=""1.0"" encoding=""utf-8""?>");
            sb.AppendLine("<packages>");
            foreach (var s in source)
                sb.AppendLine(" "+s.ToString());
            sb.AppendLine("</packages>");
            return sb.ToString();
        }       
    }
}
