﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReactiveUI;

namespace nuget.packages
{
    public class NugetPackage : ReactiveObject,IComparable
    {
        private string _Id;
        public string Id 
        {
            get { return _Id; }
            set { this.RaiseAndSetIfChanged(x => x.Id, value); }
        }
        private string _Version;
        public string Version 
        {
            get { return _Version; }
            set { this.RaiseAndSetIfChanged(x => x.Version, value); }
        }
        private string _AllowedVersions;
        public string AllowedVersions 
        {
            get { return _AllowedVersions; }
            set { this.RaiseAndSetIfChanged(x => x.AllowedVersions, value); }
        }
        private string _TargetFramework;
        public string TargetFramework 
        {
            get { return _TargetFramework; }
            set { this.RaiseAndSetIfChanged(x => x.TargetFramework, value); }
        }

        public string Project { get; private set; }

        public NugetPackage(string id, string version, string allowedVersion, string targetFramework, string project)
        {
            Id = id;
            Version = version;
            AllowedVersions = allowedVersion;
            TargetFramework = targetFramework;
            Project = project;
        }

        public int CompareTo(object obj)
        {
            var compareTo = obj as NugetPackage;
            if (compareTo != null)
            {
                if (compareTo.Id == Id && Version == compareTo.Version)
                    return 0;
                else return 1;
            }
            else
                return -1;
        }

        public override string ToString()
        {
            string asString = string.Empty;
            if(!string.IsNullOrEmpty(Id))
                asString += string.Format(@" id=""{0}""", Id);
            if (!string.IsNullOrEmpty(Version))
                asString += string.Format(@" version=""{0}""",Version);
            if (!string.IsNullOrEmpty(AllowedVersions))
                asString += string.Format(@" allowedVersions=""{0}""", AllowedVersions);
            if (!string.IsNullOrEmpty(TargetFramework))
                asString += string.Format(@" targetFramework=""{0}""", TargetFramework);

            return "<package"+asString+"/>";
        }
    }
}
